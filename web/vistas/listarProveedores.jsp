<%-- 
    Document   : listar
    
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="Modelo.Proveedores"%>
<%@page import="ModeloDAO.ProveedoresDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Proveedores</title>
    </head>
    <body>
        <h1>Listado de los Proveedores de la base de datos</h1>
        <a class="btn btn-success" href="ControladorProveedores?accion=add">Adicionar Nuevo Proveedore </a>
        <table class="table table-bordered">
        <thead><!-- comment -->
                <th class="text_center">ID</th>
                <th class="text_center">DNI</th>
                <th class="text_center">Nombres</th>
                <th class="text_center">Celular</th>
                <th class="text_center">Correo</th>
        </thead>
        <%
        ProveedoresDAO dao=new ProveedoresDAO();
        List<Proveedores> list = dao.listar();
        Iterator<Proveedores> iter=list.iterator();
        Proveedores per=null;
        while(iter.hasNext()){
            per=iter.next();
        %>
        <tbody>
            <tr>
                <td class="text_center"><%= per.getId() %> </td>
                <td class="text_center"><%= per.getDNI() %> </td>
                <td class="text_center"><%= per.getNombres() %> </td>
                <td class="text_center"><%= per.getCelular()%> </td>
                <td class="text_center"><%= per.getCorreo()%> </td>
                <td class="text center" >
                    <a class="btn btn-warning" href="ControladorProveedores?accion=editar&id=<%=per.getId()%>"> 
                        Editar
                    </a>
                    <a class="btn btn-danger" href="ControladorProveedores?accion=eliminar&id=<%=per.getId()%>">
                        Remover
                    </a>
                </td>
            </tr>
        </tbody>
           <% } %> 
        </table>
    </body>
</html>
