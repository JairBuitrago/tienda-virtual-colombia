<%-- 
    Document   : listar
    
--%>

<%@page import="Modelo.Ventas"%>
<%@page import="ModeloDAO.VentasDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Ventas</title>
    </head>
    <body>
        <h1>Listado de las Ventas de la base de datos</h1>
        <a class="btn btn-success" href="ControladorVentas?accion=add">Adicionar Nueva Venta </a>
        <table class="table table-bordered">
        <thead><!-- comment -->
                <th class="text_center">Producto_Id</th>
                <th class="text_center">Clientes_Id</th>
                <th class="text_center">Fecha</th>
                <th class="text_center">PrecioVenta</th>
                
        </thead>
        <%
        VentasDAO dao=new VentasDAO();
        List<Ventas> list = dao.listar();
        Iterator<Ventas> iter=list.iterator();
        Ventas per=null;
        while(iter.hasNext()){
            per=iter.next();
        %>
        <tbody>
            <tr>
                <td class="text_center"><%= per.getProducto_Id()%> </td>
                <td class="text_center"><%= per.getClientes_Id()%> </td>
                <td class="text_center"><%= per.getFecha()%> </td>
                <td class="text_center"><%= per.getPrecioVenta()%> </td>
                
                <td class="text center" >
                    <a class="btn btn-warning" href="ControladorVentas?accion=editar&id=<%=per.getProducto_Id()%>"> 
                        Editar
                    </a>
                    <a class="btn btn-danger" href="ControladorVentas?accion=eliminar&id=<%=per.getProducto_Id()%>">
                        Remover
                    </a>
                </td>
            </tr>
        </tbody>
           <% } %> 
        </table>
    </body>
</html>
