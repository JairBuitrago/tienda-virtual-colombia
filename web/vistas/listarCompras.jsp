<%-- 
    Document   : listar hiuuu
    
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="Modelo.Compras"%>
<%@page import="ModeloDAO.ComprasDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Compras</title>
    </head>
    <body>
        <h1>Listado de las compras de la base de datos</h1>
        <a class="btn btn-success" href="ControladorCompras?accion=add">Adicionar Nueva Compra </a>
        <table class="table table-bordered">
        <thead><!-- comment -->
                <th class="text_center">Producto_Id</th>
                <th class="text_center">Proveedores_Id</th>
                <th class="text_center">Fecha</th>
                <th class="text_center">PrecioCompra</th>
                
        </thead>
        <%
        ComprasDAO dao=new ComprasDAO();
        List<Compras> list = dao.listar();
        Iterator<Compras> iter=list.iterator();
        Compras per=null;
        while(iter.hasNext()){
            per=iter.next();
        %>
        <tbody>
            <tr>
                <td class="text_center"><%= per.getProducto_Id()%> </td>
                <td class="text_center"><%= per.getProveedores_Id()%> </td>
                <td class="text_center"><%= per.getFecha()%> </td>
                <td class="text_center"><%= per.getPrecioCompra()%> </td>
                
                <td class="text center" >
                    <a class="btn btn-warning" href="ControladorCompras?accion=editar&id=<%=per.getProducto_Id()%>"> 
                        Editar
                    </a>
                    <a class="btn btn-danger" href="ControladorCompras?accion=eliminar&id=<%=per.getProducto_Id()%>">
                        Remover
                    </a>
                </td>
            </tr>
        </tbody>
           <% } %> 
        </table>
    </body>
</html>
