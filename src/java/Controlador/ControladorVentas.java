/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controlador;

import Modelo.Ventas;
import ModeloDAO.VentasDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author HP_210
 */
public class ControladorVentas extends HttpServlet {

    String listarVentas="vistas/listarVentas.jsp";
    String add="vistas/add.jsp";
    String edit="vistas/edit.jsp";
    
    Ventas p = new Ventas();
    VentasDAO dao = new VentasDAO();
    //int id;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorVentas</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorVentas at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String acceso="";
        String action = request.getParameter("accion");
        if (action.equalsIgnoreCase("listar")){
            acceso=listarVentas;
        } else {
            if (action.equalsIgnoreCase("add")){
                acceso=add;
            } else {
                if(action.equalsIgnoreCase("Agregar")){
                   
                    int cli_id = Integer.parseInt(request.getParameter(""));
                    int preVent = Integer.parseInt(request.getParameter(""));
                    String fec =request.getParameter("");
                    
                    
                    p.setClientes_Id(cli_id);
                    p.setPrecioVenta(preVent);
                    p.setFecha(fec);
                    
                    dao.add(p);
                    acceso=listarVentas;
                } else {
                    if (action.equalsIgnoreCase("editar")){
                        request.setAttribute("idpro", request.getParameter(""));
                        acceso=edit;
                    } else {
                        if (action.equalsIgnoreCase("Actualizar")){
                            int prod_id = Integer.parseInt(request.getParameter(""));
                            int cli_id  = Integer.parseInt(request.getParameter(""));
                            int preVent = Integer.parseInt(request.getParameter(""));
                            String fec =request.getParameter("");
                            
                          
                            p.setProducto_Id(prod_id);
                            p.setClientes_Id(cli_id);
                            p.setPrecioVenta(preVent);
                            p.setFecha(fec);
                            
                            dao.edit(p);
                            acceso=listarVentas;
                        } else {
                            if (action.equalsIgnoreCase("eliminar")){
                                int prod_id = Integer.parseInt(request.getParameter("prod_id"));
                                p.setProducto_Id(prod_id);
                                dao.eliminar(prod_id);
                                acceso=listarVentas;
                            }
                        }
                    }
                }
            }
        }
        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
