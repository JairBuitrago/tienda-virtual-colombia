/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;


import Modelo.Clientes;

import java.util.List;

/**
 *
 * @author Rogerio Beltran Cast
 */
public interface CRUDClientes {
    public List listar();//Trae una lista de personas desde la base de datis

    /**
     *
     * @param id
     * @return
     */
    public Clientes list(int id);//Trae una sola persona que cumple la condición del ID
    public boolean add(Clientes a);//Adicionar una persona a la base de datos
    public boolean edit(Clientes a);//Actualiza la información de la persona en la base de datos
    public boolean eliminar(int id);//Elimina una persona en la base de datos
    
   
}

/*
C ----> Create Crear información: Insertar datos: add
R ----> Read   Leer información: Listar: Genera toda la información,list: Trae un dato
U ----> Update Editar la información de un registro: edit
D ----> Delete Eliminar un registro: eliminar
*/