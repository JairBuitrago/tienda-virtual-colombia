/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;


import Modelo.Compras;

import java.util.List;

/**
 *
 * @author Rogerio Beltran Cast
 */
public interface CRUDCompras {
    public List listar();//Trae una lista de personas desde la base de datis

    /**
     *
     * @param producto_Id
     * @return
     */
    public Compras list(int producto_Id);//Trae una sola persona que cumple la condición del ID
    public boolean add(Compras a);//Adicionar una persona a la base de datos
    public boolean edit(Compras a);//Actualiza la información de la persona en la base de datos
    public boolean eliminar(int producto_Id);//Elimina una persona en la base de datos
    
   
}

/*
C ----> Create Crear información: Insertar datos: add
R ----> Read   Leer información: Listar: Genera toda la información,list: Trae un dato
U ----> Update Editar la información de un registro: edit
D ----> Delete Eliminar un registro: eliminar
*/