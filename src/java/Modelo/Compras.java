/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.time.format.DateTimeFormatter;

/**
 *
 * 
 */
public class Compras {
    private int producto_Id;
    private int proveedores_Id;
    private String fecha;
    private int precioCompra;
    

    public Compras() {
    }

    public Compras(int producto_Id, int proveedores_Id, String fecha, int precioCompra) {
        this.producto_Id = producto_Id;
        this.proveedores_Id = proveedores_Id;
        this.fecha = fecha;
        this.precioCompra = precioCompra;
    }

    public int getProducto_Id() {
        return producto_Id;
    }

    public void setProducto_Id(int producto_Id) {
        this.producto_Id = producto_Id;
    }

    public int getProveedores_Id() {
        return proveedores_Id;
    }

    public void setProveedores_Id(int proveedores_Id) {
        this.proveedores_Id = proveedores_Id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(int precioCompra) {
        this.precioCompra = precioCompra;
    }

    
    


   

   
    
    
}
