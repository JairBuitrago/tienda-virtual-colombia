/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Jair Buitrago
 */
public class Producto {
    private int id;
    private String DNI;
    private String Nombres;
    private int Unidades;
    private String Clasificacion;

    public Producto() {
    }

    public Producto(int id, String DNI, String Nombres, int Unidades, String Clasificacion) {
        this.id = id;
        this.DNI = DNI;
        this.Nombres = Nombres;
        this.Unidades = Unidades;
        this.Clasificacion = Clasificacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public int getUnidades() {
        return Unidades;
    }

    public void setUnidades(int Unidades) {
        this.Unidades = Unidades;
    }

    public String getClasificacion() {
        return Clasificacion;
    }

    public void setClasificacion(String Clasificacion) {
        this.Clasificacion = Clasificacion;
    }

    



   

   
    
    
}
