/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.time.format.DateTimeFormatter;

/**
 *
 * 
 */
public class Ventas {
    private int producto_Id;
    private int clientes_Id;
    private String fecha;
    private int precioVenta;
    

    public Ventas() {
    }

    public Ventas(int producto_Id, int clientes_Id, String fecha, int precioVenta) {
        this.producto_Id = producto_Id;
        this.clientes_Id = clientes_Id;
        this.fecha = fecha;
        this.precioVenta = precioVenta;
    }

    public int getProducto_Id() {
        return producto_Id;
    }

    public void setProducto_Id(int producto_Id) {
        this.producto_Id = producto_Id;
    }

    public int getClientes_Id() {
        return clientes_Id;
    }

    public void setClientes_Id(int clientes_Id) {
        this.clientes_Id = clientes_Id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(int precioVenta) {
        this.precioVenta = precioVenta;
    }

    

    
    


   

   
    
    
}
