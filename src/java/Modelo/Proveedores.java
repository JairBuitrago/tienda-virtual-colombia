/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 
 */
public class Proveedores {
    private int id;
    private String DNI;
    private String Nombres;
    private String Celular;
    private String Correo;

    public Proveedores() {
    }

    public Proveedores(int id, String DNI, String Nombres, String Celular, String Correo) {
        this.id = id;
        this.DNI = DNI;
        this.Nombres = Nombres;
        this.Celular = Celular;
        this.Correo = Correo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getCelular() {
        return Celular;
    }

    public void setCelular(String Celular) {
        this.Celular = Celular;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    

   
    
    
}
