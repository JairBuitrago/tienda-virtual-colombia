/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDAO;

import Config.Conexion;
import Interface.CRUDProveedores;
import Modelo.Proveedores;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 */
public class ProveedoresDAO implements CRUDProveedores{
    Conexion cn = new Conexion(); // Conexion a la base de datos PErsona
    Connection con; // Conexion entre java y motor de base de datos
    PreparedStatement ps;// El encargado de asociar la consulta a la base de datos
    ResultSet rs;// Es donde queda alojada la información de la consulta
    Proveedores p = new Proveedores();

    /**
     *
     * @return
     */
    @Override
    public List listar() {
        ArrayList<Proveedores> list =  new ArrayList<>();
        String sentenciaSQL ="Select * from proveedores";
        try{
            //Conexión a la base de datos
            con=cn.getConnection();// Crea la conexion a la base de datos
            ps=con.prepareStatement(sentenciaSQL); // Realiza la consulta SQL en la base de datos
            rs=ps.executeQuery();//Ejecuta la consulta y almacena el resultado en el ResultSet=rs
            while(rs.next()){
                Proveedores per = new Proveedores();//Creamos el objeto PErsona
                //Asociamos la información de la consulta con los atributos de persona
                per.setDNI(rs.getString("DNI"));
                per.setId(rs.getInt("Id"));
                per.setNombres(rs.getString("Nombres"));
                per.setCelular(rs.getString("Celular"));
                per.setCorreo(rs.getString("Correo"));
                // Adicionar la persona a la lista de personas
                list.add(per);   
            }    
        } catch (Exception e){
            System.out.println("Error a listar las Productos");
        }

        return list;
    }

    @Override
    public Proveedores list(int id) {
        String sql="Select * from where id="+id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs=ps.executeQuery();// Consultar la información la base de datos
            while (rs.next()){
                p.setDNI(rs.getString("DNI"));
                p.setId(rs.getInt("ID"));
                p.setNombres(rs.getString("Nombres"));
                p.setCelular(rs.getString("Celular"));
                p.setCorreo(rs.getString("Correo"));
            }
        } catch (Exception e){
            System.out.println("Error en la consulta:"+e);
        }
        return p;
    }

    @Override
    public boolean add(Proveedores a) {
        String sql ="insert into producto (DNI,Nombres,Celular,Correo) values ('"+a.getDNI()+"','"+a.getNombres()+"','"+a.getCelular()+"','"+a.getCelular()+"')";
        try {
            con = cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();// Modificar la información de la base de datos
        } catch (Exception e){
            System.out.println("Error a adicionar Proveedores:"+e);
        }
        return false;
    }

    @Override
    public boolean edit(Proveedores a) {
        String sql ="update producto set DNI='"+a.getDNI()+"',Nombres='"+a.getNombres()+"',Celular='"+a.getCelular()+"',Correo='"+a.getCorreo()+"' where Id="+a.getId();
        //           update persona set DNI="  91473456"    ,Nombres="  PEDRO PEREZ     " where Id=  1;
        try {
            con = cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e){
            System.out.println("Error a actulizar Proveedores:"+e);
        }
        return false;
    }

    @Override
    public boolean eliminar(int id) {
        String sql ="delete from proveedores where Id="+id;
        //    delete from persona where id=5
        try {
            con = cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e){
            System.out.println("Error a elminar Proveedores:"+e);
        }
        return false;    
    }

 
    
}
