/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDAO;

import Config.Conexion;
import Interface.CRUDVentas;
import Modelo.Ventas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


 
public class VentasDAO implements CRUDVentas{
    Conexion cn = new Conexion(); // Conexion a la base de datos PErsona
    Connection con; // Conexion entre java y motor de base de datos
    PreparedStatement ps;// El encargado de asociar la consulta a la base de datos
    ResultSet rs;// Es donde queda alojada la información de la consulta
    Ventas p = new Ventas();

    /**
     *
     * @return
     */
    @Override
    public List listar() {
        ArrayList<Ventas> list =  new ArrayList<>();
        String sentenciaSQL ="Select * from ventas";
        try{
            //Conexión a la base de datos
            con=cn.getConnection();// Crea la conexion a la base de datos
            ps=con.prepareStatement(sentenciaSQL); // Realiza la consulta SQL en la base de datos
            rs=ps.executeQuery();//Ejecuta la consulta y almacena el resultado en el ResultSet=rs
            while(rs.next()){
                Ventas per = new Ventas();//Creamos el objeto PErsona
                //Asociamos la información de la consulta con los atributos de persona
                per.setProducto_Id(rs.getInt("Producto_Id"));
                per.setClientes_Id(rs.getInt("Clientes_Id"));
                per.setFecha(rs.getString("Fecha"));
                per.setPrecioVenta(rs.getInt("PrecioVenta"));
               
                // Adicionar la persona a la lista de personas
                list.add(per);   
            }    
        } catch (Exception e){
            System.out.println("Error a listar las Ventas");
        }

        return list;
    }

    @Override
    public Ventas list(int producto_Id) {
        String sql="Select * from where ventas_Id="+producto_Id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs=ps.executeQuery();// Consultar la información la base de datos
            while (rs.next()){
                p.setProducto_Id(rs.getInt("Producto_Id"));
                p.setClientes_Id(rs.getInt("Clientes_Id"));
                p.setFecha(rs.getString("Fecha"));
                p.setPrecioVenta(rs.getInt("PrecioVenta"));
                
            }
        } catch (Exception e){
            System.out.println("Error en la consulta:"+e);
        }
        return p;
    }

    @Override
    public boolean add(Ventas a) {
        String sql ="insert into ventas (Producto_Id,Clientes_Id,Fecha,precioVenta) values ('"+a.getProducto_Id()+"','"+a.getClientes_Id()+"','"+a.getFecha()+"','"+a.getPrecioVenta()+"')";
        try {
            con = cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();// Modificar la información de la base de datos
        } catch (Exception e){
            System.out.println("Error a adicionar Venta:"+e);
        }
        return false;
    }

    @Override
    public boolean edit(Ventas a) {
        String sql ="update ventas set clientes_Id='"+a.getClientes_Id()+"',fecha='"+a.getFecha()+"',PrecioVenta='"+a.getPrecioVenta()+"' where producto_Id='"+a.getProducto_Id();
        //           update persona set DNI="  91473456"    ,Nombres="  PEDRO PEREZ     " where Id=  1;
        try {
            con = cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e){
            System.out.println("Error a actulizar Venta:"+e);
        }
        return false;
    }

    @Override
    public boolean eliminar(int producto_Id) {
        String sql ="delete from ventas where producto_Id="+producto_Id;
        //    delete from persona where id=5
        try {
            con = cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e){
            System.out.println("Error a elminar Venta:"+e);
        }
        return false;    
    }

   

 
    
}
