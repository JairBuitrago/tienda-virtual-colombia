/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDAO;

import Config.Conexion;
import Interface.CRUDCompras;
import Modelo.Compras;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * 
 */
public class ComprasDAO implements CRUDCompras{
    Conexion cn = new Conexion(); // Conexion a la base de datos PErsona
    Connection con; // Conexion entre java y motor de base de datos
    PreparedStatement ps;// El encargado de asociar la consulta a la base de datos
    ResultSet rs;// Es donde queda alojada la información de la consulta
    Compras p = new Compras();

    /**
     *
     * @return
     */
    @Override
    public List listar() {
        ArrayList<Compras> list =  new ArrayList<>();
        String sentenciaSQL ="Select * from compras";
        try{
            //Conexión a la base de datos
            con=cn.getConnection();// Crea la conexion a la base de datos
            ps=con.prepareStatement(sentenciaSQL); // Realiza la consulta SQL en la base de datos
            rs=ps.executeQuery();//Ejecuta la consulta y almacena el resultado en el ResultSet=rs
            while(rs.next()){
                Compras per = new Compras();//Creamos el objeto PErsona
                //Asociamos la información de la consulta con los atributos de persona
                per.setProducto_Id(rs.getInt("Producto_Id"));
                per.setProveedores_Id(rs.getInt("Proveedores_Id"));
                per.setFecha(rs.getString("Fecha"));
                per.setPrecioCompra(rs.getInt("PrecioCompra"));
               
                // Adicionar la persona a la lista de personas
                list.add(per);   
            }    
        } catch (Exception e){
            System.out.println("Error a listar las Productos");
        }

        return list;
    }

    @Override
    public Compras list(int producto_Id) {
        String sql="Select * from where producto_Id="+producto_Id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs=ps.executeQuery();// Consultar la información la base de datos
            while (rs.next()){
                p.setProducto_Id(rs.getInt("Producto_Id"));
                p.setProveedores_Id(rs.getInt("Proveedores_Id"));
                p.setFecha(rs.getString("Fecha"));
                p.setPrecioCompra(rs.getInt("PrecioCompra"));
                
            }
        } catch (Exception e){
            System.out.println("Error en la consulta:"+e);
        }
        return p;
    }

    @Override
    public boolean add(Compras a) {
        String sql ="insert into compras (Producto_Id,Proveedores_Id,Fecha,precioCompra) values ('"+a.getProducto_Id()+"','"+a.getProveedores_Id()+"','"+a.getFecha()+"','"+a.getPrecioCompra()+"')";
        try {
            con = cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();// Modificar la información de la base de datos
        } catch (Exception e){
            System.out.println("Error a adicionar Compra:"+e);
        }
        return false;
    }

    @Override
    public boolean edit(Compras a) {
        String sql ="update compras set proveedores_Id='"+a.getProveedores_Id()+"',fecha='"+a.getFecha()+"',PrecioCompra='"+a.getPrecioCompra()+"' where producto_Id='"+a.getProducto_Id();
        //           update persona set DNI="  91473456"    ,Nombres="  PEDRO PEREZ     " where Id=  1;
        try {
            con = cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e){
            System.out.println("Error a actulizar Compra:"+e);
        }
        return false;
    }

    @Override
    public boolean eliminar(int producto_Id) {
        String sql ="delete from compras where producto_Id="+producto_Id;
        //    delete from persona where id=5
        try {
            con = cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e){
            System.out.println("Error a elminar Compra:"+e);
        }
        return false;    
    }

 
    
}
