<%-- 
    Document   : listar
    Created on : 9/09/2022, 6:14:57 a. m.
    Author     : Rogerio Beltran Cast
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="Modelo.Producto"%>
<%@page import="ModeloDAO.ProductoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Producto</title>
    </head>
    <body>
        <h1>Listado de los productos de la base de datos</h1>
        <a class="btn btn-success" href="Controlador?accion=add">Adicionar Nuevo Producto </a>
        <table class="table table-bordered">
        <thead><!-- comment -->
                <th class="text_center">ID</th>
                <th class="text_center">DNI</th>
                <th class="text_center">Nombres</th>
                <th class="text_center">Unidades</th>
                <th class="text_center">Clasificacion</th>
        </thead>
        <%
        ProductoDAO dao=new ProductoDAO();
        List<Producto> list = dao.listar();
        Iterator<Producto> iter=list.iterator();
        Producto per=null;
        while(iter.hasNext()){
            per=iter.next();
        %>
        <tbody>
            <tr>
                <td class="text_center"><%= per.getId() %> </td>
                <td class="text_center"><%= per.getDNI() %> </td>
                <td class="text_center"><%= per.getNombres() %> </td>
                <td class="text_center"><%= per.getUnidades()%> </td>
                <td class="text_center"><%= per.getClasificacion() %> </td>
                <td class="text center" >
                    <a class="btn btn-warning" href="Controlador?accion=editar&id=<%=per.getId()%>"> 
                        Editar
                    </a>
                    <a class="btn btn-danger" href="Controlador?accion=eliminar&id=<%=per.getId()%>">
                        Remover
                    </a>
                </td>
            </tr>
        </tbody>
           <% } %> 
        </table>
    </body>
</html>
