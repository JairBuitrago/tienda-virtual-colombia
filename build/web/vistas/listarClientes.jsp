<%-- 
    Document   : listar
    
--%>

<%@page import="Modelo.Clientes"%>
<%@page import="ModeloDAO.ClientesDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Clientes</title>
    </head>
    <body>
        <h1>Listado de los Clientes de la base de datos</h1>
        <a class="btn btn-success" href="ControladorClientes?accion=add">Adicionar Nuevo Cliente </a>
        <table class="table table-bordered">
        <thead><!-- comment -->
                <th class="text_center">ID</th>
                <th class="text_center">DNI</th>
                <th class="text_center">Nombres</th>
                
        </thead>
        <%
        ClientesDAO dao=new ClientesDAO();
        List<Clientes> list = dao.listar();
        Iterator<Clientes> iter=list.iterator();
        Clientes per=null;
        while(iter.hasNext()){
            per=iter.next();
        %>
        <tbody>
            <tr>
                <td class="text_center"><%= per.getId() %> </td>
                <td class="text_center"><%= per.getDNI() %> </td>
                <td class="text_center"><%= per.getNombres() %> </td>
                
                <td class="text center" >
                    <a class="btn btn-warning" href="ControladorClientes?accion=editar&id=<%=per.getId()%>"> 
                        Editar
                    </a>
                    <a class="btn btn-danger" href="ControladorClientes?accion=eliminar&id=<%=per.getId()%>">
                        Remover
                    </a>
                </td>
            </tr>
        </tbody>
           <% } %> 
        </table>
    </body>
</html>
