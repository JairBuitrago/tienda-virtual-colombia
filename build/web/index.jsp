<%-- 
    Document   : index
    
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mi Barrio: Tienda Virtual</title>
    </head>
    <body>
        <h1>Bienvenidos a mi Mi Barrio: Tienda Virtual</h1>
        
        <br><!-- comment -->
        <br><!-- comment -->
        <br><!-- comment -->
        <div class="container">
            <a class="btn btn-success btn-lg" href="ControladorProducto?accion=listar">Productos </a>
            <br><!-- comment -->
            <a class="btn btn-success btn-lg" href="ControladorProveedores?accion=listar">Proveedores </a>
            <br><!-- comment -->
            <a class="btn btn-success btn-lg" href="ControladorClientes?accion=listar">Clientes </a>
            <br><!-- comment -->
            <a class="btn btn-success btn-lg" href="ControladorCompras?accion=listar">Compras </a>
            <br><!-- comment -->
            <a class="btn btn-success btn-lg" href="ControladorVentas?accion=listar">Ventas</a>
        </div>
    </body>
</html>
